$(document).ready(function (){

    //1
    //alert("Strona załadowana !");
    //end 1

    //2
    $("#ad2").delay(3 * 1000).slideUp();
    //end 2

    //3
    $("#ad3Btn").click(function(){
        $("#ad3").toggle(0.3 * 1000);
    });
    //end3

    //4
    $("#ad4Font").click(function(){
        let value = $("#select").val();
        $(".ad4").css("color", value);
    });

    $("#ad4Back").click(function(){
        let value = $("#select").val();
        $(".ad4").css("background-color", value);
    });
    //end4

    //5
    $("#ad5Btn").click(function(){
        let value = $("#ad5Text").val();
        $(".ad5").text(value);
    });
    //end5

    //6
    $(".ad6Btn").click(function(){
        let className = $(this).attr("data");
        let target = $("#ad6");

        target.removeClass();
        target.addClass(className);
    });
    //end6

    //7
    $("#ad7hide").click(function(){
        let delay = 300;
        let elements = $("td").get();
        elements.reverse();
        elements = $(elements);
        elements.each(function(){
            var element = $(this);
            setTimeout(function(){
                element.css("display","none");
            },delay)
            delay+=300;
        });
    });

    $("#ad7show").click(function(){
        let delay = 300;
        let elements = $("td");
        elements.each(function(){
            var element = $(this);
            setTimeout(function(){
                element.css("display","table-cell");
            },delay)
            delay+=300;
        });
    });
    //end7

    //8
    var nowDate = new Date();
    var endDate = new Date("04/04/2021");
    var diff = endDate.getTime() - nowDate.getTime();
    var days = Math.ceil(diff/(1000*3600*24));

    $("#ad8").text(days);
    $("#ad8Toggle").click(function(){
        $("#ad8").parent().toggle(200);
    });

    //end8

    //9
    $("#ad9").mouseover(function(){ $(".hr").hide(100); });
    $("#ad9").mouseout(function(){ $(".hr").show(100); });
    //end 9
});
